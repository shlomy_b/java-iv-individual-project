/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import java.util.*;

import  com.mycompany.realestateapp.objects.*;

import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;



/**
 *
 * @author Shlomy_b
 */
public class TenantController implements Initializable{
    Scanner reader = new Scanner(System.in);
	static Connection c1 = getConnection();
	ObservableList<Tenant> observableList = FXCollections.observableArrayList();
    
	public TenantController() {
		getTenants();
	}

@FXML
	private TableView<Tenant> tenantView;
	
@FXML
	private TableColumn<Tenant, String> idColumn;

@FXML
	private TableColumn<Tenant, String> leaseColumn;

@FXML
	private TableColumn<Tenant, String> nameColumn;

@FXML
	private TableColumn<Tenant, String> addressColumn;

@FXML
	private TableColumn<Tenant, String> emailColumn;

@FXML
	private TableColumn<Tenant, String> numberColumn;




@FXML
	private TextField addTenant;

@FXML
	private AnchorPane createTenantAnchor;



@FXML
	private void switchToHome() throws IOException {
    App.setRoot("Home");
}
@FXML
	private void switchToProperties() throws IOException {
    App.setRoot("Properties");
}
@FXML    
	private void switchToTenants() throws IOException {
    App.setRoot("Tenants");
}
@FXML        
	private void switchToPayments() throws IOException {
    App.setRoot("Payments");
}

@FXML        
	private void switchToMaintenance() throws IOException {
	App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
	private void switchToQuit() throws IOException {
    App.setRoot("Quit");
    System.exit(0);
}
    
public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}

public void getTenants() {
	String query = "SELECT * FROM TENANTS";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		observableList.add(new Tenant(returnSet.getString("TENANT_ID"), returnSet.getString("LEASE_ID"), returnSet.getString("FULLNAME"), returnSet.getString("ADDRESS"), returnSet.getString("EMAIL"), returnSet.getString("PHONE_NUMBER")));
	}
	
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}



@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	idColumn.setCellValueFactory(new PropertyValueFactory<>("Id"));
	leaseColumn.setCellValueFactory(new PropertyValueFactory<>("LeaseId"));
	nameColumn.setCellValueFactory(new PropertyValueFactory<>("Name"));
	addressColumn.setCellValueFactory(new PropertyValueFactory<>("Address"));
	emailColumn.setCellValueFactory(new PropertyValueFactory<>("Email"));
	numberColumn.setCellValueFactory(new PropertyValueFactory<>("Number"));
	
	tenantView.setItems(observableList);
	}


@FXML           
    private void delTenant(){    	
    	Tenant item = tenantView.getSelectionModel().getSelectedItems().get(0);
    	String data = item.getName();
    	
    	try {
    	String query = "DELETE FROM TENANTS WHERE FULLNAME = ?";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, data);
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
    	
    	tenantView.getItems().removeAll(tenantView.getSelectionModel().getSelectedItems());
    }
    
    
@FXML           
    private void addTenant() throws IOException{
    	//observableList.add(new Tenant(addTenant.getText(), null));
	
		Parent root = FXMLLoader.load(getClass().getResource("CreateTenant.fxml"));
		Scene scene = new Scene(root);
    	   
    	   Stage stage = new Stage();
           stage.setTitle("S & B Housing");
           stage.setScene(scene);
           stage.show();
           
           //tenantView.getColumns().get(0).setVisible(false);
           //tenantView.getColumns().get(0).setVisible(true);
    }


@FXML           
	private void openTenant(ActionEvent event) throws IOException{
		Tenant item = tenantView.getSelectionModel().getSelectedItems().get(0);		// add something to check if they didnt select
																					// before pressing open
		String tenantId = item.getId();
		String name = item.getName();
		String address = item.getAddress();
		String phone = item.getNumber();
		String email = item.getEmail();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("OpenTenant.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
	
		OpenTenantController t1 = loader.getController();
		t1.setTenantInfo(tenantId, name, address, phone, email);

		Stage stage = new Stage();
		stage.setTitle("S & B Housing");
		stage.setScene(scene);
		stage.show();
	}
}











