/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import  com.mycompany.realestateapp.objects.*;


import java.io.IOException;

import javafx.fxml.*;
import javafx.stage.Stage;

import java.util.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import javafx.event.ActionEvent;

/**
 *
 * @author Shlomy_b
 */
public class PropertyController implements Initializable{
	static Connection c1 = getConnection();
	ObservableList<Property> observableList = FXCollections.observableArrayList();
	
	public PropertyController() {
		getProperties();
	}

@FXML
	private TableView<Property> tableView;
	
@FXML
	private TableColumn<Property, String> propertyID;

@FXML
	private TableColumn<Property, String> unitNumber;

@FXML
	private TableColumn<Property, String> vacancy;

@FXML
	private TableColumn<Property, String> propertyType;

@FXML
	private TableColumn<Property, String> propertyAddress;


@FXML
	private void switchToHome() throws IOException {
    	App.setRoot("Home");
}
@FXML
	private void switchToProperties() throws IOException {
    	App.setRoot("Properties");
}
@FXML    
	private void switchToTenants() throws IOException {
		App.setRoot("Tenants");
}
@FXML        
	private void switchToPayments() throws IOException {
    	App.setRoot("Payments");
}

@FXML        
	private void switchToMaintenance() throws IOException {
		App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
	private void switchToQuit() throws IOException {
    	App.setRoot("Quit");
    	System.exit(0);
}

public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}


public void getProperties() {
	String query = "SELECT * FROM PROPERTIES";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		observableList.add(new Property(returnSet.getString("PROPERTY_ID"), returnSet.getString("UNIT_NUMBER"), returnSet.getString("VACANT"), returnSet.getString("PROPERTY_TYPE"), returnSet.getString("PROPERTY_ADDRESS")));
	}
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}


@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	propertyID.setCellValueFactory(new PropertyValueFactory<>("propertyID"));
	unitNumber.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));
	vacancy.setCellValueFactory(new PropertyValueFactory<>("status"));
	propertyType.setCellValueFactory(new PropertyValueFactory<>("type"));
	propertyAddress.setCellValueFactory(new PropertyValueFactory<>("address"));

	tableView.setItems(observableList);
	}

@FXML           
    private void delProperty(){
    	Property item = tableView.getSelectionModel().getSelectedItems().get(0);
    	String data = item.getPropertyID();
    	
    	try {
    	String query = "DELETE FROM PROPERTIES WHERE PROPERTY_ID = ?";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, data);
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
    	tableView.getItems().removeAll(tableView.getSelectionModel().getSelectedItems());
	}
    

@FXML           
    private void addProperty() throws IOException{
		initialize(null, null);
    	//observableList.add(new Property(addProperty.getText(), null, null));
		Parent root = FXMLLoader.load(getClass().getResource("CreateProperty.fxml"));
		Scene scene = new Scene(root);
	   
	   Stage stage = new Stage();
       stage.setTitle("S & B Housing");
       stage.setScene(scene);
       stage.show();
    }

 
@FXML           
	private void openProperty(ActionEvent event) throws IOException{
		ArrayList<String> extraInfo = new ArrayList<String>();
	
		Property item = tableView.getSelectionModel().getSelectedItems().get(0);		// add something to check if they didnt select
																						// before pressing open
		String propertyId = item.getPropertyID();
		String unitNumberId = item.getUnitNumber();
		String vacancyId = item.getStatus();
		String propertyTypeId = item.getType();
		String propertyAddressId = item.getAddress();
		
		
		String query = "SELECT SCHOOL_TAX, ((AMOUNT_LEFT * MORTGAGE_RATE) / MORTGAGE_TERM) AS PER_YEAR FROM MORTGAGES\r\n" + 
				"JOIN UTILITIES USING (PROPERTY_ID)\r\n" + 
				"WHERE PROPERTY_ID = ?";
		
		try {
			PreparedStatement p1 =c1.prepareStatement(query);
			p1.setString(1, propertyId);
			ResultSet returnSet = p1.executeQuery();
			
		while(returnSet.next()) {
			extraInfo.add(returnSet.getString("PER_YEAR")); 
			extraInfo.add(returnSet.getString("SCHOOL_TAX"));
		}
		
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print(e);
		}
		
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("OpenProperty.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
		
		OpenPropertyController o1 = loader.getController();
		o1.setPropertyInfo(propertyId, unitNumberId, vacancyId, propertyTypeId, propertyAddressId, extraInfo.get(0), extraInfo.get(1));

		Stage stage = new Stage();
		stage.setTitle("S & B Housing");
		stage.setScene(scene);
		stage.show();	

	}
}















