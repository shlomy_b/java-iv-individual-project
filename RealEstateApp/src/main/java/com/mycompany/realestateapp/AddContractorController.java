package com.mycompany.realestateapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class AddContractorController {
	static Connection c1 = getConnection();
	
	@FXML
	private AnchorPane createContractorAnchor;

	@FXML
	private TextField createContractorName;
	
	@FXML
	private TextField createContractorAddress;
	
	@FXML
	private TextField createContractorPhone;
	
	@FXML
	private TextField createContractorEmail;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	public void createContractorMethod() {
		System.out.println("Contractor Added");
		
    	try {
    	String query = "INSERT INTO MAINTENANCE \r\n" + 
    			"		VALUES (?, ?, ?, ?, ?);";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, "0");
    	p1.setString(2, createContractorName.getText());
    	p1.setString(3, createContractorAddress.getText());
    	p1.setString(4, createContractorPhone.getText());
    	p1.setString(5, createContractorEmail.getText());
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
	}
}
