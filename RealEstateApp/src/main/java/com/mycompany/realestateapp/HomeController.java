/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;


import com.mycompany.realestateapp.objects.Lease;
import com.mycompany.realestateapp.objects.Property;
import com.mycompany.realestateapp.objects.Tenant;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.application.HostServices;

import javafx.application.Application;

/**
 *
 * @author Shlomy_b
 */
public class HomeController extends Application implements Initializable {
	static Connection c1 = getConnection();
	ObservableList<Lease> leaseList = FXCollections.observableArrayList();

	public HomeController() {
		getLeases();
	}
	
	
	@FXML
		private TableView<Lease> leaseTable;
	
	@FXML
		private TableColumn<Lease, String> leaseTableId;
	
	@FXML
		private TableColumn<Lease, String> leaseTablePropertyId;
	
	@FXML
		private TableColumn<Lease, String> leaseTableTenantId;
	
	@FXML
		private TableColumn<Lease, String> leaseTableUnitNumber;
	
	@FXML
		private TableColumn<Lease, String> leaseTableLeaseFile;
	
    
	@FXML
    	private void switchToHome() throws IOException {
        	App.setRoot("Home");
    }
	@FXML
		private void switchToProperties() throws IOException {
			App.setRoot("Properties");
    }
	@FXML    
    	private void switchToTenants() throws IOException {
			App.setRoot("Tenants");
    }
	@FXML        
    	private void switchToPayments() throws IOException {
        	App.setRoot("Payments");
    }

	@FXML        
		private void switchToMaintenance() throws IOException {
		App.setRoot("Maintenance");
}

	@FXML        
		private void switchToMortgage() throws IOException {
			App.setRoot("Mortgages");
}

	@FXML           
    	private void switchToQuit() throws IOException {
			App.setRoot("Quit");
			System.exit(0);
    }

public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}


public void getLeases() {
	String query = "SELECT * FROM LEASES";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		leaseList.add(new Lease(returnSet.getString("LEASE_ID"), returnSet.getString("PROPERTY_ID"), returnSet.getString("TENANT_ID"), returnSet.getString("UNIT_NUMBER"), returnSet.getString("RENT"), returnSet.getString("LEASE_FILE")));
	}
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}


@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	leaseTableId.setCellValueFactory(new PropertyValueFactory<>("LeaseId"));
	leaseTablePropertyId.setCellValueFactory(new PropertyValueFactory<>("PropertyId"));
	leaseTableTenantId.setCellValueFactory(new PropertyValueFactory<>("TenantId"));
	leaseTableUnitNumber.setCellValueFactory(new PropertyValueFactory<>("UnitNumber"));
	leaseTableLeaseFile.setCellValueFactory(new PropertyValueFactory<>("LeaseFile"));
	
		leaseTable.setItems(leaseList);
	}




@FXML           
	private void openLease(ActionEvent event) throws IOException{
		Lease item = leaseTable.getSelectionModel().getSelectedItems().get(0);		// add something to check if they didnt select
																				// before pressing open
		String leaseId = item.getLeaseId();
		String propertyId = item.getPropertyId();
		String tenantId = item.getTenantId();
		String unitNumber = item.getUnitNumber();
		String rent = item.getRent();
		String leaseFile = item.getLeaseFile();
	
	
		FXMLLoader loader = new FXMLLoader(getClass().getResource("OpenHome.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);

		OpenHomeController l1 = loader.getController();
		l1.setLeaseInfo(leaseId, propertyId, tenantId, unitNumber, rent, leaseFile);

		Stage stage = new Stage();
		stage.setTitle("S & B Housing");
		stage.setScene(scene);
		stage.show();
	}

	
@FXML           
	private void deleteLease(){
		Lease item = leaseTable.getSelectionModel().getSelectedItems().get(0);
		String data = item.getLeaseId();
	
		try {
			String query = "DELETE FROM LEASES WHERE LEASE_ID = ?";
			PreparedStatement p1 = c1.prepareStatement(query);
			p1.setString(1, data);
			p1.executeUpdate();
	
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print(e);
		}
		leaseTable.getItems().removeAll(leaseTable.getSelectionModel().getSelectedItems());
	}


	public void openLeaseFile() {
		Lease item = leaseTable.getSelectionModel().getSelectedItems().get(0);
		String leaseFile = item.getLeaseFile();
		
		HostServices hostServices = getHostServices();
        hostServices.showDocument(leaseFile);
	}
	
	@FXML	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
