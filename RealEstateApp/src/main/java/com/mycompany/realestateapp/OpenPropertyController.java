package com.mycompany.realestateapp;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.*;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class OpenPropertyController {
	static Connection c1 = getConnection();

	
	@FXML
	private AnchorPane openPropertyAnchor;

	@FXML
	private TextField openPropertyId;
	
	@FXML
	private TextField openPropertyUnit;
																
	@FXML
	private TextField openPropertyVacant;
	
	@FXML
	private TextField openPropertyType;
	
	@FXML
	private TextField openPropertyAddress;
	
	@FXML
	private TextField openPropertyMortgage;
	
	@FXML
	private TextField openPropertyTax;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	
@FXML	
	public void setPropertyInfo(String id, String unit, String vacancy, String type, String address, String yearlyAmount, String schoolTax) {
		this.openPropertyId.setText(id);
		this.openPropertyUnit.setText(unit);	
		this.openPropertyType.setText(type);
		this.openPropertyAddress.setText(address);
		this.openPropertyMortgage.setText(yearlyAmount);
		this.openPropertyTax.setText(schoolTax);
		
		if(vacancy.equals("0")) {
			this.openPropertyVacant.setText("Empty");
		}else {
			this.openPropertyVacant.setText("Rented");
		}
	}

//to update Property info
	public void updateProperty() {
		try {
		String query = "UPDATE PROPERTIES \r\n" +  
		" SET  \r\n" +   
		"    UNIT_NUMBER = ?, \r\n" + 
		"    VACANT = ?, \r\n" +
		"    PROPERTY_TYPE = ?, \r\n" +
		"    PROPERTY_ADDRESS = ? \r\n" +
		" WHERE PROPERTY_ID = ?";

		PreparedStatement p1 = c1.prepareStatement(query);					
		p1.setString(1, this.openPropertyUnit.getText());
		if(this.openPropertyVacant.getText().equals("Empty")) {
			p1.setInt(2, 1);
		}else {
			p1.setInt(2, 0);
		}
		p1.setString(3, this.openPropertyType.getText());
		p1.setString(4, this.openPropertyAddress.getText());
		p1.setString(5, this.openPropertyId.getText());
		p1.executeUpdate();
		
 	} catch (SQLException e) {
 		e.printStackTrace();
 		System.out.print(e);
 		}
	}
}


