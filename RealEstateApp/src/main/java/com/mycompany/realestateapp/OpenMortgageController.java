package com.mycompany.realestateapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;


public class OpenMortgageController {
	static Connection c1 = getConnection();

	
	@FXML
	private TextField openMortgageId;

	@FXML
	private TextField openMortgageBank;
																
	@FXML
	private TextField openMortgageAmount;
	
	@FXML
	private TextField openMortgageAmountLeft;
	
	@FXML
	private TextField openMortgageRate;
	
	@FXML
	private TextField openMortgageStartDate;
	
	@FXML
	private TextField openMortgageRenewDate;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	
@FXML	
	public void setMortgageInfo(String propertyId, String bank, String amount, String amountLeft, String mortgageRate, String startDate, String renewDate) {
		this.openMortgageId.setText(propertyId);	
		this.openMortgageBank.setText(bank);
		this.openMortgageAmount.setText(amount);
		this.openMortgageAmountLeft.setText(amountLeft);
		this.openMortgageRate.setText(mortgageRate);
		this.openMortgageStartDate.setText(startDate);
		this.openMortgageRenewDate.setText(renewDate);
	}

// to update mortgage info
	public void updateMortgage() {
		try {
		String query = "UPDATE MORTGAGES \r\n" +  
		" SET  \r\n" +   
		"    BANK = ?, \r\n" + 
		"    AMOUNT = ?, \r\n" +
		"    AMOUNT_LEFT = ?, \r\n" +
		"    MORTGAGE_RATE = ?, \r\n" +
		"    START_DATE = ?, \r\n" +
		"    RENEW_DATE = ? \r\n" +
		" WHERE PROPERTY_ID = ?";

    	PreparedStatement p1 = c1.prepareStatement(query);					
    	p1.setString(1, this.openMortgageBank.getText());
    	p1.setString(2, this.openMortgageAmount.getText());
    	p1.setString(3, this.openMortgageAmountLeft.getText());
    	p1.setString(4, this.openMortgageRate.getText());
    	p1.setString(5, this.openMortgageStartDate.getText());
    	p1.setString(6, this.openMortgageRenewDate.getText());
    	p1.setString(7, this.openMortgageId.getText());
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
	}
	
			// ADD DELETE MORTGAGE
}



