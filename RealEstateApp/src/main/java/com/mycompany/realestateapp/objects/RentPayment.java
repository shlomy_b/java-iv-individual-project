package com.mycompany.realestateapp.objects;

public class RentPayment {
	private String tenantId;
	private String amount;
	private String date;

	
    public RentPayment(String tenantId, String amount, String date) {
    	this.tenantId = tenantId;
    	this.amount = amount;
    	this.date = date;
    }
    
    
    public String getTenantId() {
    	return this.tenantId;
    }
	
    
    public String getAmount() {
    	return this.amount;
    }
    
    public String getDate() {
    	return this.date;
    }
    
   
    public void setTenantId(String tenantId) {
    	this.tenantId = tenantId;
    }
   
    
    public void setAmount(String amount) {
    	this.amount = amount;
    }
    
    public void setDate(String date) {
    	this.date = date;
    }
}
