package com.mycompany.realestateapp.objects;

public class MaintenancePayment {
	private String recordId;
	private String propertyId;
	private String contractorId;
	private String date;
	private String cost;
	private String notes;

	
    public MaintenancePayment(String recordId, String propertyId, String contractorId, String date, String cost, String notes) {
    	this.recordId = recordId;
    	this.propertyId = propertyId;
    	this.contractorId = contractorId;
    	this.date = date;
    	this.cost = cost;
    	this.notes = notes;
    }
    
    
    public String getRecordId() {
    	return this.recordId;
    }
	
    
    public String getPropertyId() {
    	return this.propertyId;
    }
    
    public String getContractorId() {
    	return this.contractorId;
    }
    
    
    public String getDate() {
    	return this.date;
    }
	
    
    public String getCost() {
    	return this.cost;
    }
    
    public String getNotes() {
    	return this.notes;
    }
    
    
    
    
    public void setRecordId(String recordId) {
    	this.recordId = recordId;
    }
    
    public void setPropertyId(String propertyId) {
    	this.propertyId = propertyId;
    }
   
    
    public void setContractorId(String contractorId) {
    	this.contractorId = contractorId;
    }
    
    public void setDate(String date) {
    	this.date = date;
    }
    
    public void setCost(String cost) {
    	this.cost = cost;
    }
    
    public void setNotes(String notes) {
    	this.notes = notes;
    }
}
