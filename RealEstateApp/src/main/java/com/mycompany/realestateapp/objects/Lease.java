/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp.objects;

/**
 *
 * @author Shlomy_b
 */
public class Lease {
	private String leaseId;
	private String propertyId;
	private String tenantId;
	private String unitNumber;
	private String rent;
	private String leaseFile;

	
    public Lease(String leaseId, String propertyId, String tenantId, String unitNumber, String rent, String leaseFile) {
    	this.leaseId = leaseId;
    	this.propertyId = propertyId;
    	this.tenantId = tenantId;
    	this.unitNumber = unitNumber;
    	this.rent = rent;
    	this.leaseFile = leaseFile;
    }
    
    
    public String getLeaseId() {
    	return this.leaseId;
    }
	
    
    public String getPropertyId() {
    	return this.propertyId;
    }
    
    public String getTenantId() {
    	return this.tenantId;
    }
    
    
    public String getUnitNumber() {
    	return this.unitNumber;
    }
	
    
    public String getRent() {
    	return this.rent;
    }
    
    public String getLeaseFile() {
    	return this.leaseFile;
    }
    
    
   
    public void setLeaseId(String leaseId) {
    	this.leaseId = leaseId;
    }
   
    
    public void setPropertyId(String propertyId) {
    	this.propertyId = propertyId;
    }
    
    public void setTenantId(String tenantId) {
    	this.tenantId = tenantId;
    }

    public void setUnitNumber(String unitNumber) {
    	this.unitNumber = unitNumber;
    }
   
    
    public void setRent(String rent) {
    	this.rent = rent;
    }
    
    public void setLeaseFile(String leaseFile) {
    	this.leaseFile = leaseFile;
    }
    
}

