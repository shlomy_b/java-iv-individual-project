package com.mycompany.realestateapp.objects;

public class MortgagePayment {
	private String mortgageId;
	private String propertyId;
	private String amount;
	private String date;
	

    public MortgagePayment(String mortgageId, String propertyId, String amount, String date) {
    	this.mortgageId = mortgageId;
    	this.propertyId = propertyId; 
    	this.amount = amount;
    	this.date = date;
    }
    
    
    public String getMortgageId() {
    	return this.mortgageId;
    }
    
    public String getPropertyId() {
    	return this.propertyId;
    }
	

    
    public String getAmount() {
    	return this.amount;
    }
    
    
    public String getDate() {
    	return this.date;
    }
    

    
    public void setPropertyId(String propertyId) {
    	this.propertyId = propertyId;
    }
   
    
    public void setMortgageId(String id) {
    	this.mortgageId = id;
    }
    
    public void setAmount(String amount) {
    	this.amount = amount;
    }
    
    
    public void setDate(String date) {
    	this.date = date;
    }
    

}

	

