package com.mycompany.realestateapp.objects;

public class Maintenace {
	private String companyName;
	private String number;
	private String email;
	private String description;

	
    public Maintenace(String name, String number, String email, String description) {
    	this.companyName = name;
    	this.number = number;
    	this.email = email;
    	this.description = description;
    	
    }
    
    
    public String getCompanyName() {
    	return this.companyName;
    }
	
    
    public String getNumber() {
    	return this.number;
    }
    
    public String getEmail() {
    	return this.email;
    }
    
    public String getDescription() {
    	return this.description;
    }
    
   
    public void setCompanyName(String name) {
    	this.companyName = name;
    }
   
    
    public void setNumber(String number) {
    	this.number = number;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }
    
    public void setdescription(String description) {
    	this.description = description;
    }
    
    
    
}



