/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp.objects;

import javafx.scene.control.Button;

/**
 *
 * @author Shlomy_b
 */
public class Tenant {
	private String tenantId;
	private String leaseId;
	private String name;
	private String address;
	private String email;
	private String number;

	
    public Tenant(String tenantId, String leaseId, String name, String address, String email, String number) {
    	this.tenantId = tenantId;
    	this.leaseId = leaseId;
    	this.address = address;
    	this.name = name;
    	this.email = email;
    	this.number = number;
    	
    }
    
    public String getId() {
    	return this.tenantId;
    }
	
    public String getLeaseId() {
    	return this.leaseId;
    }
    
    public String getAddress() {
    	return this.address;
    }
	
    
    public String getName() {
    	return this.name;
    }
    
    public String getEmail() {
    	return this.email;
    }
	
    
    public String getNumber() {
    	return this.number;
    }
    
    public void setId(String id) {
    	this.tenantId = id;
    }
    
    public void setLeaseId(String id) {
    	this.leaseId = id;
    }
    
    public void setAddress(String address) {
    	this.address = address;
    }
   
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }
   
    
    public void setNumber(String number) {
    	this.number = number;
    }
}

