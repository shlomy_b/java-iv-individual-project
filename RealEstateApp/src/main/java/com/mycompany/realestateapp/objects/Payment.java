package com.mycompany.realestateapp.objects;

public class Payment {
	private String description;
	private String amount;
	private String date;

	
    public Payment(String description, String amount, String date) {
    	this.description = description;
    	this.amount = amount;
    	this.date = date;
    }
    
    
    public String getDescription() {
    	return this.description;
    }
	
    
    public String getAmount() {
    	return this.amount;
    }
    
    public String getDate() {
    	return this.date;
    }
    
   
    public void setDescription(String description) {
    	this.description = description;
    }
   
    
    public void setAmount(String amount) {
    	this.amount = amount;
    }
    
    public void setDate(String date) {
    	this.date = date;
    }
}
