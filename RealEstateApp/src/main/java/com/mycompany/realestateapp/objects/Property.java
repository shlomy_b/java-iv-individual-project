/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp.objects;

import javafx.scene.control.*;

/**
 *
 * @author Shlomy_b
 */
public class Property {
	private String propertyID;
	private String unitNumber;
	private String status;
	private String type;
	private String address;
	
	
    public Property(String propertyID, String unitNumber, String status, String type, String address /*, Button newButton*/) {
    	this.propertyID = propertyID;
    	this.unitNumber = unitNumber;
    	this.status = status;
    	this.type = type;
    	this.address = address;
    }
    
    public void setPropertyID(String propertyID) {
    	this.propertyID = propertyID;
    }
    
    public void setUnitNumber(String unitNumber) {
    	this.unitNumber = unitNumber;
    }
    
    public void setStatus(String status) {
    	this.status = status;
    }
    
    public void setType(String type) {
    	this.type = type;
    }
    
    public void setAddress(String address) {
    	this.address = address;
    }
    
   
    public String getPropertyID() {
    	return this.propertyID;
    }
    
    public String getUnitNumber() {
    	return this.unitNumber;
    }
    
    public String getStatus() {
    	return this.status;
    }
    
    public String getType() {
    	return this.type;
    }
    
    public String getAddress() {
    	return this.address;
    }
}
