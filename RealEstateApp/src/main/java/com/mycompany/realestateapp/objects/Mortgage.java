/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp.objects;

/**
 *
 * @author Shlomy_b
 */
public class Mortgage {
	private String mortgageId;
	private String propertyId;
	private String bank;
	private String amount;
	private String amountLeft;
	private String mortgageRate;
	private String startDate;
	private String renewDate;
	private String term;
	

    public Mortgage(String mortgageId, String propertyId, String bank, String amount, String amountLeft, String mortgageRate, String startDate, String renewDate, String term) {
    	this.mortgageId = mortgageId; 
    	this.propertyId = propertyId; 
    	this.bank = bank;
    	this.amount = amount;
    	this.amountLeft = amountLeft;
    	this.mortgageRate = mortgageRate;
    	this.startDate = startDate;
    	this.renewDate = renewDate;
    	this.term = term;
    }
    
    public String getMortgageId() {
    	return this.mortgageId;
    }
    
    public String getPropertyId() {
    	return this.propertyId;
    }
	
    
    public String getBank() {
    	return this.bank;
    }
    
    public String getAmount() {
    	return this.amount;
    }
    
    public String getAmountLeft() {
    	return this.amountLeft;
    }
    
    public String getMortgageRate() {
    	return this.mortgageRate;
    }
    
    public String getStartDate() {
    	return this.startDate;
    }
    
    public String getRenewDate() {
    	return this.renewDate;
    }
    
    public String getTerm() {
    	return this.term;
    }
    
    
    public void setMortgageId(String mortgageId) {
    	this.mortgageId = mortgageId;
    }
    
    public void setPropertyId(String propertyId) {
    	this.propertyId = propertyId;
    }
   
    
    public void setBank(String bank) {
    	this.bank = bank;
    }
    
    public void setAmount(String amount) {
    	this.amount = amount;
    }
    
    
    public void setAmountLeft(String amountLeft) {
    	this.amountLeft = amountLeft;
    }
    
    
    public void setMortgageRate(String mortgageRate) {
    	this.mortgageRate = mortgageRate;
    }
    
    public void setStartDate(String startDate) {
    	this.startDate = startDate;
    }
    
    public void setRenewDate(String renewDate) {
    	this.renewDate = renewDate;
    }
    
    public void setTerm(String term) {
    	this.term = term;
    }
}



