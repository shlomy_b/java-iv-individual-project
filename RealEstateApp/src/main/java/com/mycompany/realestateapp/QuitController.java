/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import java.io.IOException;
import javafx.fxml.FXML;
/**
 *
 * @author Shlomy_b
 */
public class QuitController {

@FXML
    private void switchToHome() throws IOException {
        App.setRoot("Home");
    }
@FXML
    private void switchToProperties() throws IOException {
        App.setRoot("Properties");
    }
@FXML    
    private void switchToTenants() throws IOException {
        App.setRoot("Tenants");
    }
@FXML        
    private void switchToPayments() throws IOException {
        App.setRoot("Payments");
    }

@FXML        
	private void switchToMaintenance() throws IOException {
    App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
    private void switchToQuit() throws IOException {
        App.setRoot("Quit");
        System.exit(0);
    }
}
