/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.*;

import com.mycompany.realestateapp.objects.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;


/**
 *
 * @author Shlomy_b
 */
public class MaintenanceController implements Initializable{
	static Connection c1 = getConnection();
	ObservableList<Maintenace> observableList = FXCollections.observableArrayList();
	
	public MaintenanceController() {
		getContractors();
	}
    
@FXML
	private TableView<Maintenace> MaintenanceTable;
	
@FXML
	private TableColumn<Maintenace, String> nameColumn;

@FXML
	private TableColumn<Maintenace, String> numberColumn;

@FXML
	private TableColumn<Maintenace, String> emailColumn;

@FXML
	private TableColumn<Maintenace, String> DescriptionColumn;

@FXML
	private TextField addContractor;

	
@FXML
	private void switchToHome() throws IOException {
    App.setRoot("Home");
}
@FXML
	private void switchToProperties() throws IOException {
    App.setRoot("Properties");
}
@FXML    
	private void switchToTenants() throws IOException {
    App.setRoot("Tenants");
}
@FXML        
	private void switchToPayments() throws IOException {
    App.setRoot("Payments");
}

@FXML        
	private void switchToMaintenance() throws IOException {
	App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
	private void switchToQuit() throws IOException {
    App.setRoot("Quit");
    System.exit(0);
}

public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}


public void getContractors() {
	String query = "SELECT * FROM MAINTENANCE";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		observableList.add(new Maintenace(returnSet.getString("CONTRACTOR_NAME"), returnSet.getString("PHONE_NUMBER"), returnSet.getString("EMAIL"), returnSet.getString("JOB_DESCRIPTION")));
	}
	
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}

@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	nameColumn.setCellValueFactory(new PropertyValueFactory<>("CompanyName"));
	numberColumn.setCellValueFactory(new PropertyValueFactory<>("number"));
	emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
	DescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
	
	MaintenanceTable.setItems(observableList);
	}


@FXML           
    private void delContractor(){
    	Maintenace item = MaintenanceTable.getSelectionModel().getSelectedItems().get(0);
    	String data = item.getCompanyName();
    	
    	try {
    	String query = "DELETE FROM MAINTENANCE WHERE CONTRACTOR_NAME = ?";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, data);
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
    	
    	MaintenanceTable.getItems().removeAll(MaintenanceTable.getSelectionModel().getSelectedItems());
    }
    
    
@FXML           
    private void addContractor() throws IOException{
		Parent root = FXMLLoader.load(getClass().getResource("CreateContractor.fxml"));
		Scene scene = new Scene(root);
    	   
    	   Stage stage = new Stage();
           stage.setTitle("S & B Housing");
           stage.setScene(scene);
           stage.show();
    	
    }	
}
