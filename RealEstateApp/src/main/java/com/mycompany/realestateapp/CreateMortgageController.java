package com.mycompany.realestateapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.DatePicker;

public class CreateMortgageController {
	static Connection c1 = getConnection();
	

	@FXML
	private TextField createMortgageId;
	
	@FXML
	private TextField createMortgagePropertyId ;
	
	@FXML
	private TextField createMortgageBank;
	
	@FXML
	private TextField createMortgageAmount;
	
	@FXML
	private TextField createMortgageAmountLeft;
	
	@FXML
	private TextField createMortgageRate;
	
	@FXML
	private DatePicker createMortgageStartDate;
	
	@FXML
	private DatePicker createMortgageEndDate;
	
	@FXML
	private TextField createMortgageTerm;
	
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	public void createMortgage() {
		System.out.println("Tenant Added");
		
    	try {
    	String query = "INSERT INTO MORTGAGES \r\n" + 
    			"		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, createMortgageId.getText());
    	p1.setString(2, createMortgagePropertyId.getText());
    	p1.setString(3, createMortgageBank.getText());
    	p1.setString(4, createMortgageAmount.getText());
    	p1.setString(5, createMortgageAmountLeft.getText());
    	p1.setString(6, createMortgageRate.getText());
    	p1.setString(7, createMortgageStartDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    	p1.setString(8, createMortgageEndDate.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    	p1.setString(9, createMortgageTerm.getText());
		p1.executeUpdate();

		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
		
	}
}
