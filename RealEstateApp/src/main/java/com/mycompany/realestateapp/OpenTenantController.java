package com.mycompany.realestateapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class OpenTenantController {
	static Connection c1 = getConnection();


	@FXML
	private TextField openTenantId;
	
	@FXML
	private TextField openTenantFullname;
	
	@FXML
	private TextField openTenantAddress;
	
	@FXML
	private TextField openTenantPhone;
	
	@FXML
	private TextField openTenantEmail;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	@FXML	
	public void setTenantInfo(String tenantID, String name, String address, String phone, String email) {
		this.openTenantId.setText(tenantID);	
		this.openTenantFullname.setText(name);
		this.openTenantAddress.setText(address);
		this.openTenantPhone.setText(phone);
		this.openTenantEmail.setText(email);

	}
	
	
	public void updateTenant() {
		try {
			String query = "UPDATE TENANTS \r\n" +  
					" SET  \r\n" +    
					"    FULLNAME = ?, \r\n" +
					"    ADDRESS = ?, \r\n" +
					"    PHONE_NUMBER = ?, \r\n" +
					"    EMAIL = ? \r\n" +
					" WHERE TENANT_ID = ?";

			PreparedStatement p1 = c1.prepareStatement(query);					
			p1.setString(1, this.openTenantFullname.getText());
			p1.setString(2, this.openTenantAddress.getText());
			p1.setString(3, this.openTenantPhone.getText());
			p1.setString(4, this.openTenantEmail.getText());
			p1.setString(5, this.openTenantId.getText());
			p1.executeUpdate();
	
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print(e);
		}
	}
	
	
	
	
	
}
