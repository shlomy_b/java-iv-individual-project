package com.mycompany.realestateapp;

import java.awt.Button;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class CreateTenantController {
	static Connection c1 = getConnection();
	
	@FXML
	private AnchorPane createTenantAnchor;

	@FXML
	private TextField createTenantId;
	
	@FXML
	private TextField createTenantLeaseId;
	
	@FXML
	private TextField createTenantName;
	
	@FXML
	private TextField createTenantAddress;
	
	@FXML
	private TextField createTenantPhone;
	
	@FXML
	private TextField createTenantEmail;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	public void createTenantMethod() {
		System.out.println("Tenant Added");
		
    	try {
    	String query = "INSERT INTO TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) \r\n" + 
    			"		VALUES (?, ?, ?, ?, ?, ?);";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, createTenantId.getText());
    	p1.setString(2, createTenantLeaseId.getText());
    	p1.setString(3, createTenantName.getText());
    	p1.setString(4, createTenantAddress.getText());
    	p1.setString(5, createTenantPhone.getText());
    	p1.setString(6, createTenantEmail.getText());
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
		
	}
}
