package com.mycompany.realestateapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import com.mycompany.realestateapp.objects.Mortgage;
import com.mycompany.realestateapp.objects.Property;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.net.*;



public class MortgageController implements Initializable{
	static Connection c1 = getConnection();
	ObservableList<Mortgage> observableList = FXCollections.observableArrayList();
	
	
	public MortgageController() {
		getMortgages();
	}
	
	
@FXML
	private TableView<Mortgage> MortgageTable;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnMortgageId;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnID;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnBank;
	
@FXML
	private TableColumn<Mortgage, String> MortgageColumnAmount;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnBalance;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnRate;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnStartDate;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnRenewDate;

@FXML
	private TableColumn<Mortgage, String> MortgageColumnTerm;
	
	
	
@FXML
    private void switchToHome() throws IOException {
        App.setRoot("Home");
    }
@FXML
    private void switchToProperties() throws IOException {
        App.setRoot("Properties");
    }
@FXML    
    private void switchToTenants() throws IOException {
        App.setRoot("Tenants");
    }
@FXML        
    private void switchToPayments() throws IOException {
        App.setRoot("Payments");
    }

@FXML        
	private void switchToMaintenance() throws IOException {
    	App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
    private void switchToQuit() throws IOException {
        App.setRoot("Quit");
        System.exit(0);
    }

@FXML 
public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}

@FXML
public void getMortgages() {
	String query = "SELECT * FROM MORTGAGES";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

		ResultSet returnSet = p1.executeQuery();
			while(returnSet.next()) {
				observableList.add(new Mortgage(returnSet.getString("MORTGAGE_ID"), returnSet.getString("PROPERTY_ID"), returnSet.getString("BANK"), returnSet.getString("AMOUNT"), returnSet.getString("AMOUNT_LEFT"), returnSet.getString("MORTGAGE_RATE"), returnSet.getString("START_DATE"), returnSet.getString("RENEW_DATE"), returnSet.getString("MORTGAGE_TERM")));
				}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print(e);
		}
	}



@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	MortgageColumnMortgageId.setCellValueFactory(new PropertyValueFactory<>("MortgageId"));
	MortgageColumnID.setCellValueFactory(new PropertyValueFactory<>("propertyId"));
	MortgageColumnBank.setCellValueFactory(new PropertyValueFactory<>("bank"));
	MortgageColumnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
	MortgageColumnBalance.setCellValueFactory(new PropertyValueFactory<>("amountLeft"));
	MortgageColumnRate.setCellValueFactory(new PropertyValueFactory<>("mortgageRate"));
	MortgageColumnStartDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
	MortgageColumnRenewDate.setCellValueFactory(new PropertyValueFactory<>("renewDate"));
	MortgageColumnTerm.setCellValueFactory(new PropertyValueFactory<>("Term"));
	
	MortgageTable.setItems(observableList);
	}



@FXML           
private void openMortgage(ActionEvent event) throws IOException{
	Mortgage item = MortgageTable.getSelectionModel().getSelectedItems().get(0);		// add something to check if they didnt select
																					// before pressing open
	String propertyId = item.getPropertyId();
	String bank = item.getBank();
	String amount = item.getAmount();
	String amountLeft = item.getAmountLeft();
	String mortgageRate = item.getMortgageRate();
	String startDate = item.getStartDate();
	String renewDate = item.getRenewDate();
	

	FXMLLoader loader = new FXMLLoader(getClass().getResource("OpenMortgage.fxml"));
	Parent root = loader.load();
	Scene scene = new Scene(root);
	
	OpenMortgageController o1 = loader.getController();
	o1.setMortgageInfo(propertyId, bank, amount, amountLeft, mortgageRate, startDate, renewDate); // add textfield for term

	Stage stage = new Stage();
	stage.setTitle("S & B Housing");
	stage.setScene(scene);
	stage.show();	

}


@FXML           
private void deleteMortgage(){
	Mortgage item = MortgageTable.getSelectionModel().getSelectedItems().get(0);
	String data = item.getMortgageId();
	
	try {
	String query = "DELETE FROM MORTGAGES WHERE MORTGAGE_ID = ?";
	PreparedStatement p1 = c1.prepareStatement(query);
	p1.setString(1, data);
	p1.executeUpdate();
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
	MortgageTable.getItems().removeAll(MortgageTable.getSelectionModel().getSelectedItems());
}
	

	
	
	@FXML           
    private void addMortgage() throws IOException{
	
		Parent root = FXMLLoader.load(getClass().getResource("CreateMortgage.fxml"));
		Scene scene = new Scene(root);
    	   
    	   Stage stage = new Stage();
           stage.setTitle("S & B Housing");
           stage.setScene(scene);
           stage.show();
           
           //tenantView.getColumns().get(0).setVisible(false);
           //tenantView.getColumns().get(0).setVisible(true);
    }
}
