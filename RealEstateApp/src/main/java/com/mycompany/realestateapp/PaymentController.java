/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.realestateapp;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.*;
import java.util.*;

import com.mycompany.realestateapp.objects.Property;
import com.mycompany.realestateapp.objects.RentPayment;
import com.mycompany.realestateapp.objects.Tenant;
import com.mycompany.realestateapp.objects.Maintenace;
import com.mycompany.realestateapp.objects.MaintenancePayment;
import com.mycompany.realestateapp.objects.MortgagePayment;
import com.mycompany.realestateapp.objects.Payment;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Shlomy_b
 */
public class PaymentController implements Initializable{ 
	static Connection c1 = getConnection();
	ObservableList<RentPayment> rentList = FXCollections.observableArrayList();
	ObservableList<MaintenancePayment> maintenanceList = FXCollections.observableArrayList();
	ObservableList<MortgagePayment> mortgageList = FXCollections.observableArrayList();

	public PaymentController() {
		getRentPayments();
		getMaintenancePayments();
		getMortgagePayments();
	}

// All Tables
@FXML
	private TableView<RentPayment> paymentRentTable;
	
@FXML
	private TableView<MaintenancePayment> paymentMaintenanceTable;

@FXML
	private TableView<MortgagePayment> paymentMortgageTable;

@FXML
	private TableView<Payment> paymentTaxesTable;
	
	
//For rent tab	
@FXML
	private TableColumn<RentPayment, String> rentName;

@FXML
	private TableColumn<RentPayment, String> rentAmount;
	
@FXML
	private TableColumn<RentPayment, String> rentDate;



// For maintenance tab	
@FXML
	private TableColumn<MaintenancePayment, String> maintenanceRecordId;

@FXML
	private TableColumn<MaintenancePayment, String> maintenancePropertyId;

@FXML
	private TableColumn<MaintenancePayment, String> maintenanceContractorId;

@FXML
	private TableColumn<MaintenancePayment, String> maintenanceDate;

@FXML
	private TableColumn<MaintenancePayment, String> maintenanceCost;

@FXML
	private TableColumn<MaintenancePayment, String> maintenanceNotes;



// For mortgages tab	
@FXML
	private TableColumn<MortgagePayment, String> mortgageId;

@FXML
	private TableColumn<MortgagePayment, String> mortgagePropertyId;

@FXML
	private TableColumn<MortgagePayment, String> mortgageAmount;

@FXML
	private TableColumn<MortgagePayment, String> mortgageDate;


// For taxes tab
@FXML
	private TableColumn<Payment, String> taxesType;

@FXML
	private TableColumn<Payment, String> taxesAmount;

@FXML
	private TableColumn<Payment, String> taxesDate; 



@FXML
    private void switchToHome() throws IOException {
        App.setRoot("Home");
    }
@FXML
    private void switchToProperties() throws IOException {
        App.setRoot("Properties");
    }
@FXML    
    private void switchToTenants() throws IOException {
        App.setRoot("Tenants");
    }
@FXML        
    private void switchToPayments() throws IOException {
        App.setRoot("Payments");
    }

@FXML        
	private void switchToMaintenance() throws IOException {
    App.setRoot("Maintenance");
}

@FXML        
	private void switchToMortgage() throws IOException {
		App.setRoot("Mortgages");
}

@FXML           
    private void switchToQuit() throws IOException {
        App.setRoot("Quit");
        System.exit(0);
    }

public static Connection getConnection() {
	Connection conn = null;
		try{
			// * Change this String to refer to your database. 
			String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
			conn = DriverManager.getConnection(url, "root", "root");
				
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return conn; 		
}

public void getRentPayments() {
	String query = "SELECT * FROM RENT_RECORDS";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		rentList.add(new RentPayment(returnSet.getString("TENANT_ID"), returnSet.getString("AMOUNT_PAID"), returnSet.getString("DATE_PAID")));
	}
	
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}


public void getMaintenancePayments() {
	String query = "SELECT * FROM MINTENANCE_RECORDS";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		maintenanceList.add(new MaintenancePayment(returnSet.getString("RECORD_ID"), returnSet.getString("PROPERTY_ID"), returnSet.getString("CONTRACTOR_ID"), returnSet.getString("DATE"), returnSet.getString("COST"), returnSet.getString("NOTES")));
	}
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}


public void getMortgagePayments() {
	String query = "SELECT * FROM MORTGAGE_RECORDS";
	PreparedStatement p1;

	try {
		p1 = c1.prepareStatement(query);

	ResultSet returnSet = p1.executeQuery();
	while(returnSet.next()) {
		mortgageList.add(new MortgagePayment(returnSet.getString("MORTGAGE_ID"), returnSet.getString("PROPERTY_ID"), returnSet.getString("AMOUNT"), returnSet.getString("DATE_PAID")));
	}
	
	
	} catch (SQLException e) {
		e.printStackTrace();
		System.out.print(e);
	}
}


@Override
public void initialize(URL url, ResourceBundle resourceBundle) {
	rentName.setCellValueFactory(new PropertyValueFactory<>("TenantId"));
	rentAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
	rentDate.setCellValueFactory(new PropertyValueFactory<>("date"));
	
	maintenanceRecordId.setCellValueFactory(new PropertyValueFactory<>("RecordId"));
	maintenancePropertyId.setCellValueFactory(new PropertyValueFactory<>("PropertyId"));
	maintenanceContractorId.setCellValueFactory(new PropertyValueFactory<>("ContractorId"));
	maintenanceDate.setCellValueFactory(new PropertyValueFactory<>("Date"));
	maintenanceCost.setCellValueFactory(new PropertyValueFactory<>("Cost"));
	maintenanceNotes.setCellValueFactory(new PropertyValueFactory<>("Notes"));
	
	
	mortgageId.setCellValueFactory(new PropertyValueFactory<>("MortgageId"));
	mortgagePropertyId.setCellValueFactory(new PropertyValueFactory<>("PropertyId"));
	mortgageAmount.setCellValueFactory(new PropertyValueFactory<>("Amount"));
	mortgageDate.setCellValueFactory(new PropertyValueFactory<>("Date"));
	

	paymentRentTable.setItems(rentList);
	paymentMaintenanceTable.setItems(maintenanceList);
	paymentMortgageTable.setItems(mortgageList);
	}
}
