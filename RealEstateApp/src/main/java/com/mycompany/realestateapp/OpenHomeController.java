package com.mycompany.realestateapp;

import java.awt.Button;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.stage.FileChooser;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class OpenHomeController {
	static Connection c1 = getConnection();

	String fileLocation;	
	
	@FXML
	private TextField openLeaseId;

	@FXML
	private TextField openLeasePropertyId;
																
	@FXML
	private TextField openLeaseTenantId;
	
	@FXML
	private TextField openLeaseUnitNumber;
	
	@FXML
	private TextField openLeaseRent;
	
	@FXML
	private TextField openLeaseLeaseFile;
	
	
	
	Button button = new Button("Select File");
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	
@FXML	
	public void setLeaseInfo(String propertyId, String bank, String amount, String amountLeft, String mortgageRate, String startDate) {
		this.openLeaseId.setText(propertyId);	
		this.openLeasePropertyId.setText(bank);
		this.openLeaseTenantId.setText(amount);
		this.openLeaseUnitNumber.setText(amountLeft);
		this.openLeaseRent.setText(mortgageRate);
		this.openLeaseLeaseFile.setText(startDate);
	}

// To select File
	public void selectFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Downloads"));
		File selectedFile = fileChooser.showOpenDialog(null); 
		this.openLeaseLeaseFile.setText(selectedFile.getAbsolutePath());
	}

// to update Leases info
	public void updateLeases() {
		try {
		String query = "UPDATE LEASES \r\n" +  
		" SET  \r\n" +   
		"    LEASE_ID = ?, \r\n" + 
		"    PROPERTY_ID = ?, \r\n" +
		"    TENANT_ID = ?, \r\n" +
		"    UNIT_NUMBER = ?, \r\n" +
		"    RENT = ?, \r\n" +
		"    LEASE_FILE = ? \r\n" +
		" WHERE LEASE_ID = ?";

    	PreparedStatement p1 = c1.prepareStatement(query);					
    	p1.setString(1, this.openLeaseId.getText());
    	p1.setString(2, this.openLeasePropertyId.getText());
    	p1.setString(3, this.openLeaseTenantId.getText());
    	p1.setString(4, this.openLeaseUnitNumber.getText());
    	p1.setString(5, this.openLeaseRent.getText());
    	p1.setString(6, this.openLeaseLeaseFile.getText());
    	p1.setString(7, this.openLeaseId.getText());
		p1.executeUpdate();

		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
	}
}




