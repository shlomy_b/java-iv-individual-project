package com.mycompany.realestateapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class AddPropertyController {
	static Connection c1 = getConnection();
	
	@FXML
	private AnchorPane createPropertyAnchor;

	@FXML
	private TextField createPropertyUnit;
	
	@FXML
	private TextField createPropertyVacant;
	
	@FXML
	private TextField createPropertyType;
	
	@FXML
	private TextField createPropertyAddress;
	
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:mysql://127.0.0.1:3306/realestateapp";
				conn = DriverManager.getConnection(url, "root", "root");
					
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	public void createPropertyMethod() {
		System.out.println("Property Added");
		
    	try {
    	String query = "INSERT INTO PROPERTIES \r\n" + 
    			"		VALUES (?, ?, ?, ?, ?);";
    	PreparedStatement p1 = c1.prepareStatement(query);
    	p1.setString(1, "0");
    	p1.setString(2, createPropertyUnit.getText());
    	p1.setString(3, createPropertyVacant.getText());
    	p1.setString(4, createPropertyType.getText());
    	p1.setString(5, createPropertyAddress.getText());
		p1.executeUpdate();
		
    	} catch (SQLException e) {
    		e.printStackTrace();
    		System.out.print(e);
    	}
	}
}
