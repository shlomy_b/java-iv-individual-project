import static org.junit.Assert.*;

import org.junit.Test;
import com.mycompany.realestateapp.*;
import com.mycompany.realestateapp.objects.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;


public class JUnitTests {

//	@Test
//	// Test constructor and getter methods for the Tenant Class
//	public void testGettersTenant() {
//		Tenant t1 = new Tenant("Shlomy", "1234 Apple Road");
//		assertEquals("Shlomy", t1.getName());
//		assertEquals("1234 Apple Road", t1.getAddress());
//
//		//fail("Not yet implemented");
//	}
//
//	
//	@Test
//	// Test setter methods for the Tenant Class
//	public void testSettersTenant() {
//		Tenant t2 = new Tenant("Shlomy", "1234 Apple Road");
//
//		t2.setName("Mike");
//		t2.setAddress("123 tree road");
//		
//		assertEquals("Mike", t2.getName());		
//		assertEquals("123 tree road", t2.getAddress()); 
//
//		//fail("Not yet implemented");
//	}

	
//	@Test
//	// Test setter methods for the Property Class
//	public void testGettersProperty() {
//		Property p1 = new Property("123 willow road", "apartement", "Rented Out");
//		
//		assertEquals("123 willow road", p1.getAddress());
//		assertEquals("apartement", p1.getType());
//		assertEquals("Rented Out", p1.getStatus());
//		
//	}
	
	
//	@Test
//	// Test setter methods for the Tenant Class
//	public void testSettersProperty() {
//		Property p2 = new Property("5635 van horne", "apartement", "Rented Out");
//
//		p2.setAddress("2222 lemiux");
//		p2.setType("Home");
//		p2.setStatus("empty");
//		
//		assertEquals("2222 lemiux", p2.getAddress());		
//		assertEquals("Home", p2.getType()); 
//		assertEquals("empty", p2.getStatus()); 
//
//		//fail("Not yet implemented");
//	}

	
	@Test
	// Test constructor and getter methods for the Maintenance Class
	public void testGettersMaintenance() {
		Maintenace m1 = new Maintenace("Bob the Builder", "514-111-4444", "bob@gmail.com", "General");
		
		assertEquals("Bob the Builder", m1.getCompanyName());
		assertEquals("514-111-4444", m1.getNumber());
		assertEquals("bob@gmail.com", m1.getEmail());
		assertEquals("General", m1.getDescription());

		//fail("Not yet implemented");
	}

	
	@Test
	// Test setter methods for the Maintenance Class
	public void testSettersMaintenance() {
		Maintenace m2 = new Maintenace("mike pluming", "514-222-3333", "mike@gmail.com", "plumbing");

		m2.setCompanyName("big plumbers");
		m2.setNumber("532-456-3453");
		m2.setEmail("big@gmail.com");
		m2.setdescription("general");
		
		assertEquals("big plumbers", m2.getCompanyName());
		assertEquals("532-456-3453", m2.getNumber());
		assertEquals("big@gmail.com", m2.getEmail());
		assertEquals("general", m2.getDescription());

//		fail("Not yet implemented");
	}

}
