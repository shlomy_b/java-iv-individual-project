-- RUN THIS TO POPULATE DATABASE TABLES
 
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (1, 1, 'Alon Minchinton', '35 Golden Leaf Point', '(198) 8509440', 'aminchinton0@japanpost.jp');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (2, 2, 'Ryon Fardy', '5010 Westerfield Avenue', '(727) 6415399', 'rfardy1@wisc.edu');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (3, 3, 'Anders McGonagle', '8095 Lien Avenue', '(177) 9948506', 'amcgonagle2@wordpress.org');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (4, 4, 'Meagan Johannesson', '84712 Center Parkway', '(353) 2427175', 'mjohannesson3@buzzfeed.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (5, 5, 'Henry McNabb', '815 Erie Hill', '(903) 8144825', 'hmcnabb4@ovh.net');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (6, 6, 'Bethina Ather', '536 Dennis Avenue', '(788) 2162316', 'bather5@xing.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (7, 7, 'Addy Downs', '957 Arizona Crossing', '(740) 4810086', 'adowns6@intel.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (8, 8, 'Cristine Digginson', '59308 Annamark Drive', '(253) 6663473', 'cdigginson7@google.ca');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (9, 9, 'Alfie Belfelt', '68898 Glacier Hill Terrace', '(456) 8768689', 'abelfelt8@nps.gov');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (10, 10, 'Shannon Magwood', '17120 Bobwhite Parkway', '(585) 9348369', 'smagwood9@prlog.org');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (11, 11, 'Aldon Pavlenko', '820 Charing Cross Pass', '(265) 6407158', 'apavlenkoa@wix.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (12, 12, 'Gustav Joll', '393 Moulton Place', '(680) 4799083', 'gjollb@spotify.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (13, 13, 'Gerrard Lafayette', '47 Pearson Circle', '(361) 6451237', 'glafayettec@infoseek.co.jp');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (14, 14, 'Sileas Candwell', '1890 Morningstar Terrace', '(329) 1672981', 'scandwelld@e-recht24.de');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (15, 15, 'Ilka Morrilly', '91384 Northland Circle', '(270) 3739347', 'imorrillye@google.es');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (16, 16, 'Shurlock Dunstan', '79 Logan Terrace', '(979) 9856611', 'sdunstanf@cafepress.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (17, 17, 'Chere Wynne', '87847 Pond Road', '(688) 6844068', 'cwynneg@issuu.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (18, 18, 'Evered Walesby', '67 Moulton Circle', '(988) 4929740', 'ewalesbyh@tripadvisor.com');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (19, 19, 'Amanda Kirley', '22 Bay Terrace', '(220) 6227949', 'akirleyi@tiny.cc');
insert into TENANTS (TENANT_ID, LEASE_ID, FULLNAME, ADDRESS, PHONE_NUMBER, EMAIL) values (20, 20, 'Henriette Dovinson', '3095 Jackson Lane', '(310) 2814082', 'hdovinsonj@irs.gov');



INSERT INTO MAINTENANCE 
VALUES ('1', 'SHOIR DRYWALLERS', "514-234-4352", 'shoir@gmail.ca', "interior");
INSERT INTO MAINTENANCE 
VALUES ('2', 'SHOIR DRYWALLERS', "514-234-4352", "shoir@gmail.ca", "interior");
INSERT INTO MAINTENANCE
VALUES ('3', 'MAVEN LANSCAPING', "514-721-7245", "maven@gmail.ca", "landscaping");
INSERT INTO MAINTENANCE 
VALUES ('4', "NEXUS LIGHTING", "514-728-7855", "nexus@gmail.ca", "lighting");
INSERT INTO MAINTENANCE 
VALUES ('5', "23 PLUMMING", "513-453-3455", "airp@gmail.ca", "plumbing");
INSERT INTO MAINTENANCE 
VALUES ('6', "TEEE DRYWALLERS", "514-234-4352", "shoir@gmail.ca", "interior");
INSERT INTO MAINTENANCE
VALUES ('7', "COOL LANSCAPING", "514-721-7245", "maven@gmail.ca", "landscaping");
INSERT INTO MAINTENANCE 
VALUES ('8', "FUN LIGHTING", "514-728-7855", "nexus@gmail.ca", "lighting");
INSERT INTO MAINTENANCE 
VALUES ('9', "RED PLUMMING", "513-453-3455", "airp@gmail.ca", "plumbing");
INSERT INTO MAINTENANCE 
VALUES ('10', "MINO DRYWALLERS", "514-234-4352", "shoir@gmail.ca", "interior");


INSERT INTO PROPERTIES 
VALUES ('1', '101', 0, 'CONDO', "3040 Sherbrooke St W, Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES 
VALUES ('2', '102', 0, "CONDO", "34 Decarie, Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES
VALUES ('3', '103', 0, "CONDO", "23 Van Horne, Montreal, Quebec H2d 1A4");
INSERT INTO PROPERTIES 
VALUES ('4', "104", 1, "CONDO", "34 Decarie, Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES 
VALUES ('5', "201", 0, "CONDO", "3 Van Horne, Montreal, Quebec H2d 1A4");
INSERT INTO PROPERTIES 
VALUES ('6', "202", 0, "CONDO", "764 Sherbrooke St W, Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES
VALUES ('7', "203", 0, "CONDO", "7643 Sherbrooke , Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES 
VALUES ('8', "204", 0, "CONDO", "63 Decarie, Montreal, Quebec H3Z 1A4");
INSERT INTO PROPERTIES 
VALUES ('9', "205", 0, "CONDO", "2354 Van Horne, Montreal, Quebec H2d 1A4");
INSERT INTO PROPERTIES 
VALUES ('10', "206", 1, "CONDO", "3040 Sherbrooke St W, Montreal, Quebec H3Z 1A4");



INSERT INTO PAYMENTS 
VALUES ("Leah Paid Rent" , "$1200", "04/02/2021");
INSERT INTO PAYMENTS 
VALUES ("lili Paid Rent" , "$1800", "04/10/2021");
INSERT INTO PAYMENTS
VALUES ("Water Leak Repair unit#201", "$759.99", "01/02/2021");
INSERT INTO PAYMENTS 
VALUES ("Mortage for property (ID 04)", "$1500", "07/05/2021");

	insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (1, 1, 'Konstantine Strang', 269248.8, 253296.2, 2.3, '2020-09-22', '2025-03-16', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (2, 2, 'Haleigh Varlow', 695999.6, 282798.0, 1.5, '2020-09-23', '2024-09-13', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (3, 3, 'Vernice Debow', 536011.5, 92810.9, 2.1, '2020-06-20', '2020-09-24', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (4, 4, 'Leese McKinie', 613121.4, 458981.1, 2.9, '2020-06-25', '2024-09-16', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (5, 5, 'Dill Burroughes', 457071.5, 182141.1, 2.2, '2021-03-31', '2022-09-01', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (6, 6, 'Dael Labadini', 677091.6, 419164.1, 1.2, '2021-04-23', '2021-04-30', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (7, 7, 'Tracie Olyfant', 696732.0, 579255.9, 1.5, '2020-05-18', '2020-11-14', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (8, 8, 'Gardie Strang', 94786.3, 687767.4, 3.6, '2021-03-04', '2025-04-07', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (9, 9, 'Jeannie Quartly', 180374.3, 352616.5, 2.5, '2021-04-20', '2023-04-01', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (10, 10, 'Lorelei Divisek', 124910.7, 424764.9, 3.7, '2021-02-10', '2024-11-25', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (11, 11, 'Caren Roome', 607216.9, 318135.0, 2.5, '2020-12-15', '2022-05-29', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (12, 12, 'Barty Duigan', 376941.9, 390193.2, 1.6, '2020-12-29', '2020-10-17', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (13, 13, 'Beatriz Firks', 115085.8, 150813.0, 2.0, '2020-10-12', '2022-02-21', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (14, 14, 'Marjy Cuttelar', 445518.6, 407302.6, 3.8, '2020-06-15', '2023-01-12', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (15, 15, 'Gaye Nolleth', 456157.9, 521971.1, 1.3, '2020-09-13', '2022-08-17', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (16, 16, 'Constancy Atcock', 652827.5, 322653.8, 1.6, '2020-05-25', '2024-07-05', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (17, 17, 'Marin Maior', 592316.5, 175923.8, 2.0, '2020-11-08', '2020-07-29', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (18, 18, 'Fredra Rankcom', 191080.3, 184031.1, 2.4, '2020-09-30', '2022-02-02', 25);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (19, 19, 'Shepherd Smithe', 347759.9, 351658.0, 3.6, '2021-01-10', '2022-01-09', 30);
insert into MORTGAGES (MORTGAGE_ID, PROPERTY_ID, BANK, AMOUNT, AMOUNT_LEFT, MORTGAGE_RATE, START_DATE, RENEW_DATE, MORTGAGE_TERM) values (20, 20, 'Berke Freire', 205775.8, 148960.6, 3.7, '2021-03-04', '2022-11-01', 25);

   
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (1, 2020.3, 7780.0, 6012.4, 8124.5);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (2, 876.5, 7773.9, 7946.8, 5150.9);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (3, 1440.1, 3369.6, 6796.2, 9613.9);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (4, 1710.3, 5561.2, 3607.8, 650.1);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (5, 1304.6, 5000.5, 5892.7, 6490.3);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (6, 2426.7, 5230.1, 5346.4, 6859.6);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (7, 2328.6, 3542.5, 5690.4, 860.9);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (8, 2400.0, 6257.7, 1155.7, 9242.2);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (9, 931.4, 6683.3, 7750.6, 4878.7);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (10, 1863.8, 3179.6, 3057.7, 5939.7);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (11, 2356.3, 4798.2, 3408.3, 8741.1);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (12, 2081.1, 3380.3, 1292.2, 7888.6);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (13, 1544.1, 8945.9, 4215.5, 485.4);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (14, 1535.7, 5917.2, 7332.8, 8166.0);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (15, 1631.7, 6070.9, 6001.0, 1509.3);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (16, 2034.4, 6692.8, 3436.3, 4047.3);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (17, 2398.9, 3199.3, 1322.1, 6749.6);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (18, 1062.1, 5290.6, 1266.8, 4914.7);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (19, 2460.5, 6658.3, 3517.4, 675.8);
insert into UTILITIES (PROPERTY_ID, SCHOOL_TAX, INSURANCE, ELECTRICITY, HEATING) 
values (20, 1814.7, 4920.0, 2285.7, 2381.5);
	
    
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (1, 9, 9, '2021-02-04', 8684.6, 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (2, 4, 9, '2020-11-22', 3299.7, 'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (3, 2, 9, '2021-02-27', 4115.0, 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (4, 10, 10, '2020-07-30', 5259.2, 'Proin risus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (5, 2, 7, '2021-03-12', 6956.9, 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (6, 5, 9, '2021-01-25', 3836.1, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (7, 4, 8, '2020-08-31', 7917.1, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (8, 2, 5, '2020-07-11', 2590.1, 'Duis aliquam convallis nunc.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (9, 9, 1, '2020-12-08', 6367.8, 'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (10, 5, 10, '2020-12-31', 7873.6, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (11, 1, 10, '2020-11-20', 8074.6, 'Donec posuere metus vitae ipsum.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (12, 1, 6, '2021-02-13', 8183.4, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (13, 8, 3, '2020-05-19', 6982.0, 'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (14, 8, 7, '2021-01-20', 3681.3, 'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (15, 8, 2, '2020-09-08', 6929.4, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (16, 7, 6, '2021-02-02', 4707.2, 'Nulla ut erat id mauris vulputate elementum.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (17, 6, 8, '2020-12-02', 1157.5, 'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (18, 10, 9, '2020-07-23', 2373.4, 'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (19, 8, 4, '2020-08-25', 4372.3, 'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (20, 7, 3, '2020-08-31', 9981.9, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (21, 8, 8, '2021-04-26', 3934.8, 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (22, 4, 1, '2020-05-17', 1267.8, 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (23, 5, 7, '2021-04-24', 8026.5, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (24, 5, 6, '2020-07-04', 2060.5, 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (25, 3, 8, '2021-04-03', 438.7, 'Pellentesque at nulla. Suspendisse potenti.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (26, 6, 5, '2021-04-19', 7704.6, 'Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (27, 1, 8, '2020-12-08', 864.2, 'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (28, 10, 1, '2021-03-01', 7794.9, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (29, 9, 5, '2020-07-27', 8736.6, 'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (30, 5, 4, '2021-05-12', 324.2, 'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (31, 4, 5, '2021-04-01', 8043.6, 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (32, 7, 3, '2021-02-16', 4124.2, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (33, 1, 6, '2020-10-10', 9383.0, 'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (34, 9, 5, '2021-04-15', 1143.3, 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (35, 3, 1, '2020-12-09', 8711.7, 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (36, 8, 3, '2021-02-03', 1078.0, 'Aliquam sit amet diam in magna bibendum imperdiet.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (37, 8, 8, '2021-02-13', 789.1, 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (38, 8, 5, '2020-07-22', 8614.2, 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (39, 3, 2, '2021-01-16', 2296.1, 'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.');
insert into MINTENANCE_RECORDS (RECORD_ID, PROPERTY_ID, CONTRACTOR_ID, DATE, COST, NOTES) 
values (40, 6, 2, '2021-01-28', 3849.1, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.');


insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (6, 1616.0, '2020-09-19');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (5, 1934.3, '2020-09-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (5, 1473.2, '2021-01-04');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (8, 2073.4, '2020-05-18');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (5, 1700.4, '2020-08-09');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (2, 2880.1, '2021-05-12');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (7, 946.5, '2021-04-26');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (10, 1321.5, '2021-01-26');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (4, 2881.0, '2020-06-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 2709.3, '2020-12-12');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (5, 1018.3, '2020-12-13');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (3, 1014.9, '2020-06-13');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 2111.4, '2020-08-18');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (3, 1288.6, '2020-12-11');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (8, 719.3, '2021-01-18');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (10, 1944.8, '2021-04-30');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (7, 1694.5, '2021-05-12');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (4, 1049.4, '2021-01-23');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (6, 2211.0, '2020-05-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (6, 2530.5, '2021-03-29');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 2595.5, '2020-12-24');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 2162.0, '2021-02-23');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (5, 1513.5, '2020-12-16');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (2, 1811.0, '2020-07-29');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (2, 1092.8, '2020-10-23');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (1, 769.0, '2020-08-09');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (1, 2264.7, '2020-05-31');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (4, 2991.6, '2021-02-14');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 2301.4, '2021-04-11');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (6, 1200.6, '2021-01-05');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (4, 1390.2, '2020-09-05');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 

values (1, 2283.0, '2020-12-24');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (10, 2446.2, '2020-07-22');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (9, 1124.8, '2021-04-08');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (1, 1245.5, '2021-01-11');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) 
values (10, 2408.5, '2021-03-01');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 2799.1, '2020-11-05');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (1, 2780.0, '2020-09-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 801.0, '2021-01-26');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 1347.6, '2020-10-25');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (2, 2643.9, '2021-02-04');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (5, 2000.1, '2021-04-06');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 1380.5, '2021-04-07');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (8, 1790.0, '2021-01-02');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (3, 1782.4, '2020-05-19');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (10, 2615.5, '2021-02-02');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (2, 1321.2, '2020-10-27');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (5, 1132.3, '2020-06-13');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 1836.7, '2021-01-08');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (10, 2574.6, '2020-10-04');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (2, 2854.0, '2020-09-10');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 724.7, '2020-07-06');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (5, 877.6, '2020-10-11');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (2, 1120.9, '2020-06-24');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (10, 1388.5, '2020-10-12');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (8, 2087.9, '2020-11-17');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 2362.1, '2021-03-11');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (3, 1704.8, '2021-01-20');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 1342.7, '2020-07-12');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (5, 1050.9, '2020-09-09');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (2, 1354.3, '2021-02-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (1, 2763.6, '2020-10-09');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (3, 2155.2, '2020-08-02');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (10, 1053.5, '2021-04-14');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (9, 1628.1, '2020-06-08');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 1350.5, '2020-08-23');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 1962.4, '2021-04-26');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (4, 2028.6, '2021-03-20');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 2224.0, '2020-08-04');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 1369.6, '2020-10-24');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (4, 2535.3, '2020-08-29');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (4, 1271.6, '2020-08-21');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (10, 1114.6, '2020-12-29');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 1217.2, '2021-02-06');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (8, 947.9, '2020-10-27');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (7, 2463.5, '2020-10-08');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (6, 2082.4, '2020-08-07');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (9, 2497.7, '2021-01-08');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (5, 671.8, '2020-06-02');
insert into RENT_RECORDS (TENANT_ID, AMOUNT_PAID, DATE_PAID) values (4, 1071.1, '2020-09-26');


insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (1, 9, 5, 6028.7);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (2, 8, 8, 2070.2);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (3, 10, 6, 2732.8);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (4, 5, 8, 4117.1);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (5, 8, 6, 6976.1);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (6, 2, 2, 856.7);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (7, 10, 4, 6219.0);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (8, 8, 5, 4269.0);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (9, 2, 1, 6370.6);
insert into TENANT_PROPERTY (PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT) 
values (10, 10, 5, 4916.8);

insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (1, 1, 3264.7, '2021-01-29');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (2, 2, 893.0, '2020-09-04');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (3, 3, 3049.9, '2021-05-10');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (4, 4, 1350.7, '2020-09-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (5, 5, 4614.6, '2021-05-05');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (6, 6, 4778.3, '2020-08-29');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (7, 7, 2505.5, '2020-11-29');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (8, 8, 2791.4, '2020-07-31');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (9, 9, 3488.3, '2020-11-15');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (10, 10, 5234.3, '2020-05-30');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (11, 11, 3140.8, '2020-12-13');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (12, 12, 4908.0, '2021-01-23');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (13, 13, 2067.4, '2020-10-21');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (14, 14, 5921.1, '2020-10-04');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (15, 15, 3481.4, '2020-10-19');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (16, 16, 4815.8, '2020-07-15');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (17, 17, 3361.4, '2021-03-25');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (18, 18, 2764.6, '2021-03-09');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (19, 19, 3928.1, '2021-05-13');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (20, 20, 4750.7, '2020-07-11');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (21, 21, 1159.9, '2020-12-15');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (22, 22, 2781.9, '2020-06-08');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (23, 23, 742.5, '2020-09-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (24, 24, 2378.8, '2021-02-28');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (25, 25, 4433.4, '2020-08-24');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (26, 26, 1612.8, '2021-04-15');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (27, 27, 4627.0, '2021-02-27');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (28, 28, 3073.9, '2020-10-22');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (29, 29, 3125.9, '2021-04-23');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (30, 30, 3903.1, '2021-04-06');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (31, 31, 4188.5, '2021-04-11');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (32, 32, 5307.2, '2021-04-09');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (33, 33, 2375.6, '2021-05-10');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (34, 34, 2623.8, '2020-11-27');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (35, 35, 5321.6, '2020-07-24');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (36, 36, 3820.2, '2020-11-23');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (37, 37, 3929.9, '2021-04-12');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (38, 38, 1591.3, '2021-03-30');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (39, 39, 582.8, '2020-07-04');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (40, 40, 2128.0, '2020-12-21');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (41, 41, 5416.9, '2020-11-24');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (42, 42, 1353.4, '2020-09-30');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (43, 43, 1414.5, '2020-12-12');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (44, 44, 5239.2, '2020-10-31');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (45, 45, 1170.8, '2021-03-08');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (46, 46, 5709.7, '2021-05-14');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (47, 47, 3659.7, '2020-12-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (48, 48, 2347.6, '2020-06-11');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (49, 49, 3320.7, '2021-01-25');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (50, 50, 2018.4, '2020-07-13');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (51, 51, 1852.1, '2021-01-20');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (52, 52, 1192.3, '2020-10-26');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (53, 53, 3027.7, '2021-02-18');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (54, 54, 1205.2, '2020-07-09');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (55, 55, 4405.1, '2020-10-21');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (56, 56, 5896.3, '2021-05-04');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (57, 57, 683.3, '2020-09-10');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (58, 58, 799.2, '2020-07-11');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (59, 59, 3442.6, '2021-01-19');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (60, 60, 4825.3, '2021-02-08');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (61, 61, 940.5, '2020-08-21');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (62, 62, 2498.8, '2021-05-09');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (63, 63, 3862.3, '2020-10-13');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (64, 64, 3068.7, '2020-05-23');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (65, 65, 3752.6, '2021-05-14');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (66, 66, 4344.7, '2021-03-02');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (67, 67, 4587.4, '2020-06-09');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (68, 68, 3148.3, '2020-07-08');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (69, 69, 5365.9, '2021-05-03');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (70, 70, 4779.2, '2021-01-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (71, 71, 5344.3, '2021-01-19');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (72, 72, 5638.8, '2020-11-04');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (73, 73, 4903.2, '2020-10-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (74, 74, 5086.0, '2021-02-28');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (75, 75, 3168.7, '2020-06-06');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (76, 76, 5322.9, '2021-05-01');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (77, 77, 4466.1, '2020-05-23');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (78, 78, 1859.8, '2021-01-22');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (79, 79, 4221.2, '2020-06-21');
insert into MORTGAGE_RECORDS (MORTGAGE_ID, PROPERTY_ID, AMOUNT, DATE_PAID) values (80, 80, 5976.2, '2020-05-23');

insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (1, 1, 1, 669, 5979, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (2, 2, 2, 277, 2651, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (3, 3, 3, 174, 2510, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (4, 4, 4, 354, 2271, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (5, 5, 5, 502, 1848, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (6, 6, 6, 423, 6551, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (7, 7, 7, 407, 5734, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (8, 8, 8, 534, 6253, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (9, 9, 9, 694, 5658, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (10, 10, 10, 280, 1632, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (11, 11, 11, 173, 6130, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (12, 12, 12, 103, 2354, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (13, 13, 13, 384, 5615, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (14, 14, 14, 476, 1319, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (15, 15, 15, 671, 4289, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (16, 16, 16, 288, 4411, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (17, 17, 17, 303, 6098, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (18, 18, 18, 201, 2004, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (19, 19, 19, 137, 1718, null);
insert into LEASES (LEASE_ID, PROPERTY_ID, TENANT_ID, UNIT_NUMBER, RENT, LEASE_FILE) values (20, 20, 20, 536, 3924, null);
